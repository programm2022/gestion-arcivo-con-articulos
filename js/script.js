// PROCESO PARA LA LECTURA DE ARCHIVO CSV
fetch("listas.csv")
    .then(function (res) {        //en res nos regresa el contenido
        console.log(res);
        return (res.text());   //retorno el contenido en forma de texto
    })
    .then(function (data) { //con el contenido recibido  armamos una funcion que lo aplico
        console.log(data);
        mostrarTabla(data);
    });

function mostrarTabla(contenido) {
    let tabular = document.querySelector(".contenedor");
    let filas = contenido.split(/\r?\n|\r/);

    let template = ``;
    for (let i = 0; i < filas.length; i++) {
        let celdasFila = filas[i].split(',');
        template += `<article>`
        template += `<img src=" ${celdasFila[0]}" alt=""/>`
        template += `<h4>${celdasFila[1]}</h4>`
        template += `</article>`
        console.log("---------------------------------");
        console.log(template);

    }

    tabular.innerHTML = template;
}


fetch("listas2.csv")
    .then(function (res) {        //en res nos regresa el contenido
        console.log(res);
        return (res.text());   //retorno el contenido en forma de texto
    })
    .then(function (data) { //con el contenido recibido  armamos una funcion que lo aplico
        console.log(data);
        mostrarTable(data);
    });

function mostrarTable(contenido) {
    let tabular = document.querySelector(".telefonia-celular");
    let filas = contenido.split(/\r?\n|\r/);

    let template = ``;
    for (let i = 0; i < filas.length; i++) {
        let celdasFila = filas[i].split(',');
        template += `<article>`
        template += `<img src=" ${celdasFila[0]}" alt=""/>`
        template += `<h4>${celdasFila[1]}</h4>`
        template += `</article>`
        console.log("---------------------------------");
        console.log(template);

    }
    tabular.innerHTML = template;
}
